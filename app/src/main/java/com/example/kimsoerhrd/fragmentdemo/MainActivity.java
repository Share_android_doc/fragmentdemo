package com.example.kimsoerhrd.fragmentdemo;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.kimsoerhrd.fragmentdemo.fragment.ArticleAFragment;
import com.example.kimsoerhrd.fragmentdemo.fragment.ArticleBFragment;

public class MainActivity extends AppCompatActivity implements Article {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fragmentA, new ArticleAFragment());
        ft.add(R.id.fragmentB, new ArticleBFragment());
        ft.commit();
    }

    @Override
    public void resource(String resource) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        ArticleBFragment articleBFragment = (ArticleBFragment) fragmentManager.findFragmentById(R.id.fragmentB);
        articleBFragment.updateText(resource);
    }
}
